import './App.css';
import fabriques from "./JSON/villes.json";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import { useState } from "react";
import Info from "./Info.js";
import Dep from "./Departement.js";
import L from "leaflet";








function App() {
  const [i, setI] = useState(0);
  function changerIndex(index) {
    setI(index)
  }


  
  
  const [meteo, setmeteo] = useState(null);

  function weather(lat, long) {

    var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat.toString() +
      "&lon=" + long.toString() + "&appid=c3d2917bfd71c3f3e0155b663fbad60e&lang=fr&units=metric";
  
    fetch(url).then(function (response) {
      if (response.ok) {
        response.json().then((data) => {
          setmeteo(data);
          console.log(meteo);
  

        });
      }
  
    });
  
  
  }






  var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var redIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  const [geolocationCoord, setCoord] = useState([0, 0]);
  if ("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function (position) {
      setCoord([position.coords.latitude, position.coords.longitude]);
    }
    )

  }



function iconURL(name){

  if (name === undefined) {
    return ""
  }
  else {
    return "http://openweathermap.org/img/wn/" + name + "@2x.png";
  }
}


  return (





    <div>



      <link href="./SimplonWeatherMap_files/app.css" rel="stylesheet" />
      <link href="./SimplonWeatherMap_files/app.css" rel="preload" as="style" />

      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>



        <div className="smp-faq-link-footer">
          <div className="simplebar-content ">
            <div tabIndex={0} role="button" style={{ backgroundColor: 'white', color: 'black' }} className="smp-card factory-card keyboard-focusable active"><h3 className="card-title">
              Simplon <Info num={i}></Info>
              <span className="card-title-info" /></h3> <div className="card-body"><i style={{ color: '#ce0033' }} className="material-icons">location_on</i> <div className="card-address"><Dep num={i}></Dep></div>
                <div className="smp-newsletter-faq-footer-title">Informations</div>
              </div>
            </div>
          </div>
        </div>

        <div className="App">
          <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossOrigin="" />
          <MapContainer center={[43.505, 2.35]} zoom={6}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {fabriques.map((fab, index) => {
              return (
                <Marker icon={redIcon} position={[fab.coordonnee.lat, fab.coordonnee.long]} key={index}
                  eventHandlers={{
                    click: () => {
                      changerIndex(index)
                      weather(fab.coordonnee.lat, fab.coordonnee.long)
            
                    },
                  }}
                >
                  <Popup>
                    <h2>{fab.ville}</h2>
                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                      <div style={{ paddingLeft : '10px', paddingRight : '10px'  }}>
                      <h3>Temps</h3>
    

                      <img src={  iconURL(meteo?.weather[0].icon) } alt="tot"></img>
                    <div>{meteo?.weather[0].description}</div>
                      </div>

                      <div style={{ paddingLeft : '10px', paddingRight : '10px'  }}>

                    <h3>Vent</h3>
                    <div>Vitesse : {meteo?.wind.speed}</div>
                      </div>

<div style={{ paddingLeft : '10px', paddingRight : '10px'  }}>
<h3>Infos</h3>
                    <div>Humidité  : {meteo?.main.humidity}</div>
                    <div>Pression  : {meteo?.main.pressure}</div>
                    <div>Temperature  : {meteo?.main.temp}</div>
</div>

                    </div>
                    

                  </Popup>
                </Marker>

              )

            })}


            <Marker position={geolocationCoord} icon={greenIcon}  >
              <Popup>
                <h2>Vous etes ici</h2>
                 <div className='ligneAround'>
                      <div className='paddLR'>
                      <h3>Temps</h3>
    

                      <img src={  iconURL(meteo?.weather[0].icon) } alt="img"></img>
                    <div>{meteo?.weather[0].description}</div>
                      </div>

                      <div className='paddLR'>

                    <h3>Vent</h3>
                    <div>Vitesse : {meteo?.wind.speed}</div>
                      </div>

<div style={{ paddingLeft : '10px', paddingRight : '10px'  }}>
<h3>Infos</h3>
                    <div>Humidité  : {meteo?.main.humidity}</div>
                    <div>Pression  : {meteo?.main.pressure}</div>
                    <div>Temperature  : {meteo?.main.temp}°C</div>
</div>

                    </div>
              </Popup>
            </Marker>
          </MapContainer>


        </div>
      </div>


    </div>



  );
}

export default App;